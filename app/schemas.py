from pydantic import BaseModel


class RequestBody(BaseModel):
    Label: str
