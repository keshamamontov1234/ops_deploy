import pandas as pd
from fastapi import FastAPI
from prometheus_fastapi_instrumentator import Instrumentator
from schemas import RequestBody
from utils import load_model
from dotenv import load_dotenv


load_dotenv()


model = load_model()
app = FastAPI()
Instrumentator().instrument(app).expose(app)


@app.post("/")
async def predict(body: RequestBody):
    data = body.dict()
    data['Cluster Label'] = data.pop('Label')
    data = pd.DataFrame(data, index=[0])
    print(data)
    result = model.predict(data)[0]
    print(result)
    return int(result[0])
